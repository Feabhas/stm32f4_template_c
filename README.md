# README #

### What is this repository for? ###

* Minimal C Project for the `STM32F407` Feabhas Target Board
* Version 1.0

### How do I get set up? ###

* The project contains the `feabhOS` adaption layer for `FreeRTOS`
* When cloning this project it is important that you do a recursive clone
* `git clone --recursive git@bitbucket.org:Feabhas/stm32f4_template_c.git`

### Using feabhOS/FreeRTOS ###

* The base project runs without using the RTOS functionality but still supports SysTick timing
* To use the RTOS you must set a preprocessor directive `RTOS`
* This is used in the TickTimer handling code



```
#!c
    #ifdef RTOS
      xPortSysTickHandler();
    #endif
```